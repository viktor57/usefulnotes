function Get(url: string) {
    return (target: any, name: string) {
        const secretProperty = '_$$' + name + '$$_';
        const 
        
        const init = () => {
            return fetch('dgdfgdfg').then(res => res.json());
        };

        Object.defineProperty(target, name, {
            get: function() {
                return this[secretProperty] || (this[secretProperty] = init());
            },
            configurable: true
        });
    };
};

function First() {
    return (target: any, name: string) {
        const hiddenProperty = '__$$' + name + '$$_';
        const prevInit = Object.getOwnPropertyDescriptor(target, name).get;

        const init = () => {
            return prevInit()
                .then(res => res[0]);
        };

        Object.defineProperty(target, name, {
            get: function() {
                return this[hiddenProperty] || (this.hiddenProperty = init());
            },
            configurable: true
        });
    };
};

/**
 * and finally use our decorators 
 */

interface ITodo {
    userId: number;
    id: number;
    title: string;
    completed: boolean;
}

class TodoService {
    @First()
    @Get('https://jsonplaceholder.typicode.com/todos')
    todos: Promise<ITodo[]>;
  }
  
const todoService = new TodoService();
  
todoService.todos.then(todos => {
    console.log(todos);
});