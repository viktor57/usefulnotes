const numbers = [1, 2];
const someObject = {
    id: 24, 
    name: 'Jhon'
};
const someBoolean = true;


/**
 * It doesn't need to do. TS might be used more clever.
 */
// type FlattenArray<T extends any[]> = [number];
// type FlattenObject<T extends object> = [keyof T];
// type FlattenBoolean<T> = T;   

type Flatten<T> = T extends any[] ? T[number] : T extends object[] ? T[keyof T] : T;

type NumbersArray = Flatten<typeof numbers>;
type SomeObject = Flatten<typeof someObject>;
type SomeBoolean = Flatten<typeof someBoolean>;

