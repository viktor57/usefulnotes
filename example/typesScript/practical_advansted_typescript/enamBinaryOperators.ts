enum AnimalFlags {
    None = 0,
    HasClaws = 1 << 0,
    CanFly = 1 << 1,
}

interface Animal {
    flags: any;
}

function printAnimalAbilities(animal: Animal) {
    const animalFlag = animal.flags;

    if (animalFlag & AnimalFlags.HasClaws) {
        console.log('animal has claws');
    }

    if (animalFlag & AnimalFlags.CanFly) {
        console.log('animal can fly');
    }

    if (animalFlag & AnimalFlags.None) {
        console.log('nothing');
    }
}

const animal: Animal = {flags: AnimalFlags.None};
printAnimalAbilities(animal) // nothing
animal.flags |= AnimalFlags.HasClaws;
printAnimalAbilities(animal) // animal has claws
animal.flags &= ~AnimalFlags.HasClaws;
printAnimalAbilities(animal) // nothing
animal.flags |= AnimalFlags.HasClaws | AnimalFlags.CanFly;
printAnimalAbilities(animal) // both abilities console.log