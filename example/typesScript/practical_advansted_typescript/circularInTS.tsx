interface Action {
    type: string;
}

const action1 = { type: 'login '};
const action2 = { type: 'load' };

interface ListNode<T> {
    value: T;
    next?: ListNode<T>;
    prev?: ListNode<T>;
}

const actionNode1: ListNode<Action> = {
    value: action1
};

const actionNode2: ListNode<Action> = {
    value: action2,
    prev: actionNode1
};

actionNode1.next = actionNode2;

let currentNode: ListNode<Action> | undefined = actionNode2;

do {
    console.log(currentNode.value);
    currentNode = currentNode.prev;
} while(currentNode);

/**
 * Need to typescript creates es6 module 
 */
export {};
