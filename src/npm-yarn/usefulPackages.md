# node js packages

- `pg-boss` - очередь задач для postgresql
- `mkdirp` - mkdir node
- `globby` - поиск файлов
- `execa` - bash node js
- `relative` - конструктор относительных путей
- `npm-run-all` - стартует все скрипты по шаблону
- `husky` - прекомит линтер
- `dom-loaded` - проверяет загружен ли DOM
- `lint-staged` - проверки на стадии линта
- `nodemon` - live reload сервера node js
- `yarnhook`
- `minimist` - парсинг bash комманд
- `prettier` - преттиер
- `stylelint` - линтер
- `p-memoize` - мемоизация функций
- `p-map-series` - выполняет запросы последовательно
- `p-map` - выполняет запросы синхронно
- `danger` - автоматизация git уведомлений
