# Добавление новых registry в проект

- Add `.npmrc` into root of the project
- Connect registry like sample below

```planetext

registry = https://nexus.datamind.ru/repository/npm-all/
strict-ssl = false
ca = ''

```
