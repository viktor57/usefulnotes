# DOM methods

## Console

`console.table` - выводит json файл как таблицу, добавляя отображение в виде массива
`console.assert` - _сonsole.assert(a === b)_ - срабатывает, только если сравненние неверно, плохо поддерживается браузерами
`console.count` и `console.countReset` - счечтик количества вызовов
`console.group` и `console.groupEnd` - группировка логов в консоли
`console.time` и `console.timeEnd` - замер времени работы функций и т.д.
