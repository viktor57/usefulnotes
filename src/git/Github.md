## Команды для работы с git и их расшифровки

- локальный reset отдельного файла `git checkout [path]`
- merge веток `git merge {имя ветки, которую хотим смержить с текущей}`
- ресет всей дирректории до последнего коммита `git reset --hard HEAD`
- принудительный пуш в ветку `git push --force`
- push без проверок `git push --no-verify`
- изменить существующий коммит (например название) `git commit --amend`
- объединять коммиты при мердже в ветку `skwosh commits`
- спрятать изменения `git stash`
- вернуть спрятанные изменения `git stash apply`
