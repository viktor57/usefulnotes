## Gitlab

### Deploy master to the prod

- `Repository`
- `Tags`
- `New tag`

### Reload envs after DB megrate

- `Operations`
- `Enviroments`
- `Stop enviroments`

### Import components from other repo

- `spc nexus --from bank --to datamind sync @platform-ui/input`
- `spc` это прикладные скрипты, которые не вошли в specials-cli, полный путь до скрипта. Скоро обещают открыть доступ к библиотеке платформы.
- [ссылка на скрипт](https://gitlab-ui.datamind.ru/SPEC/LABS/hq/blob/d139fc7dcb7802a62591f2363d4bf6047802681c/commands/nexus/index.js)

### Создание нового репозитория из шаблона

`https://hoverboard.project.tinkoff.ru/playground`

```javascript

mutation bootstrapRepo {
 bootstrapRepo(data:{path:“SPEC/LANDING-PAGES/tickets-backend”, template: “https://gitlab-ui.datamind.ru/SPEC/LABS/GENERATOR_OUTPUT/generator-output-typescript.git”})
}
```
