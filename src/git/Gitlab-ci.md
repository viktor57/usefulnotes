## Разбор gitlab-ci.yaml

```plaintext
---
stages:                   <= стадии сборки проекта
  - test
  - build
  - docker
  - deploy
  - stop

variables:
  NODE_VERSION: 10.15.0
  APP_CI_IMAGE_BRANCH: latest
  KUBECTL_APPLY_FORCE: "false"
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2

before_script:
  - source /opt/ci/gitlab/backend/ci.sh  <= скрипт, который живет в дирректории с репозиторией

image: eu.gcr.io/cad-special/ci:${APP_CI_IMAGE_BRANCH} <= Docker образ

test:
  stage: test         <= название стадии
  services:
    - docker:dind
  only:               <= на какое действие запускать стадию
    refs:
      - merge_requests     <= на merge request
      - master            <= или push into master
  script:                 <= скрипты для стадии
    - yarn_install        <= скрипты из package.json
    - yarn_test
  artifacts:              <= аналог export - то что мы хотим экспортировать из модуля, пайплайны
    paths:
      - node_modules/
  dependencies: []
  cache:                  <= локальный cache yarn
    paths:
      - .yarn

build:
  stage: build
  only:
    refs:
      - merge_requests
      - master
  script:
    - yarn_build
  artifacts:
    paths:
      - build/
      - packages/   # tsc transpiles inline
      - database/   # tsc transpiles inline
      - resources.json
  dependencies:
    - test

docker:
  stage: docker
  only:
    refs:
      - merge_requests
      - master
  services:
    - name: docker:dind
      command: ["dockerd", "--host=tcp://0.0.0.0:2375"]
  script:
    - gcloud_docker_auth
    - docker_build
    - docker_publish
  dependencies:
    - test
    - build

dev_deploy:
  stage: deploy
  only:
    refs:
      - merge_requests
  script:
    - gcloud_auth
    - cluster_get_credentials
    - dev_deploy
  environment:
    name: $CI_COMMIT_REF_NAME
    url: https://$CI_PROJECT_NAME-$CI_MERGE_REQUEST_IID.np.$APP_DOMAIN
    on_stop: dev_stop
  dependencies: []

dev_stop:
  stage: stop
  only:
    refs:
      - merge_requests
  when: manual
  variables:
    GIT_STRATEGY: none
  script:
    - gcloud_auth
    - cluster_get_credentials
    - dev_stop
  environment:
    name: $CI_COMMIT_REF_NAME
    url: https://$CI_PROJECT_NAME-$CI_MERGE_REQUEST_IID.np.$APP_DOMAIN
    action: stop
  dependencies: []

stage_deploy:
  stage: deploy
  only:
    refs:
      - master
  script:
    - gcloud_auth
    - cluster_get_credentials
    - stage_deploy
  environment:
    name: stage
    url: https://$CI_PROJECT_NAME.np.$APP_DOMAIN
    on_stop: stage_stop
  dependencies: []

stage_stop:
  stage: stop
  only:
    refs:
      - master
  when: manual
  variables:
    GIT_STRATEGY: none
  script:
    - gcloud_auth
    - cluster_get_credentials
    - stage_stop
  environment:
    name: stage
    url: https://$CI_PROJECT_NAME.np.$APP_DOMAIN
    action: stop
  dependencies: []

prod_deploy:
  stage: deploy
  only:
    refs:
      - tags
  script:
    - gcloud_auth
    - cluster_get_credentials
    - prod_deploy
  environment:
    name: production
    url: https://$CI_PROJECT_NAME.$APP_DOMAIN
  dependencies: []
```
