# Полезные и интересные факты о javaScript

- В javaScript существует максимальное и минимальное числовое значение - `Number.MAX_SAFE_INTEGER` и `Number.MIN_SAFE_INTEGER`.

- Так же существует максимальное числовое значение в JS - `Number.MAX_VALUE` и `Number.MIN_VALUE`.

**Все за пределами этих значение будет либо `Infinity` либо `-Infinity`**
