# Мокирование модулей

## jest --clearCache

Если вдруг что-то пошло не так и тесты не проходят, надо попробовать сбросить кеш, т.к. в некоторых упоротых случаях это вполне себе помогает.

- В дирректории с тестом `jest.mock(path-to-module);`
  **\_это позволит jest понять, что модуль замокирован и нуждается в клоне из дирректории **mocks**\_**
- В дирректории с мокируемым модулем необходимо создать **mocks** куда поместить файл с точной копией имени мокируемого файла
  **_jest увидит директорию и файл в ней, а дальше воспользуется именно им для теста_**

**Этот способ не работает рекурсивно, т.е. нельзя создавать одну дирректорию **mocks** на верхнем уровне и туда сваливать все файлы или пытаться там воссоздать fs. Не сработает**

## Jest Transform

Иногда необходимо протестировать модуль, включающий в себя js, css, ts и т.д., но default jest умеет только в js. Выход - подключить jest transform модули. Это позволит прогнать код через babel или иные loader-ы и включить недостающие файлы.

### Внимание **jest может отключать эту функцию по умолчанию для некоторых модулей, чтобы не выполнять работу понапрасну. Это конфигурируется в jest.config.js"**

- подключаем расширенные настройки в общий js.config.js

```javascript
module.exports = {
  ...require('@specials/obid/src/config/jest.config'),
  transformIgnorePatterns: ['node_modules/(?!(@platform-ui|@tinkoff|@tinkoff-ui|@specials-ui)/)'],
};
```

- общий js.config.js

```javascript
module.exports = {
  collectCoverageFrom: [
    '<rootDir>/packages/*/src/**/*.{js,jsx,mjs}',
    '!<rootDir>/node_modules/',
    '!<rootDir>/packages/frontend/src/components/plop-templates/*',
  ],
  setupFiles: [require.resolve('./setupTests.js'), require.resolve('./polyfills.js')],
  testMatch: [
    '<rootDir>/packages/**/__tests__/**/*.{js,jsx,mjs,ts,tsx}',
    '<rootDir>/packages/**/?(*.)(spec|test).{js,jsx,mjs,ts,tsx}',
  ],
  testEnvironment: 'node',
  testURL: 'http://localhost',
  transform: {
    '^.+\\.(js|jsx|mjs|ts|tsx)$': require.resolve('babel-jest'),
    '^.+\\.css$': require.resolve('./jest/cssTransform'),
    '^(?!.*\\.(js|jsx|mjs|css|json)$)': require.resolve('./jest/fileTransform'),
  },
  transformIgnorePatterns: ['node_modules/(?!(@platform-ui|@tinkoff|@tinkoff-ui)/)'],
  moduleNameMapper: {
    '^react-native$': 'react-native-web',
  },
  moduleFileExtensions: ['web.js', 'js', 'json', 'web.jsx', 'jsx', 'node', 'mjs', 'ts', 'tsx'],
};
```
